#include <ch.h>
#include <hal.h>
#include "bootloader_app_interface.hpp"

static THD_WORKING_AREA(waThread1, 128);
void Thread1(void) {
  chRegSetThreadName("blinker");

  while(1) {
    palClearPad(GPIOB, GPIOB_LED_RED);
    palSetPad(GPIOB, GPIOB_LED_GREEN);
    chThdSleepMilliseconds(500);
    palSetPad(GPIOB, GPIOB_LED_RED);
    palClearPad(GPIOB, GPIOB_LED_GREEN);
    chThdSleepMilliseconds(500);
  }
}

static const volatile struct __attribute__((packed))
{
    uint8_t signature[8]   = {'A','P','D','e','s','c','0','0'};
    uint64_t image_crc     = 0;
    uint32_t image_size    = 0;
    uint32_t vcs_commit    = GIT_HASH;
    uint8_t major_version  = FW_VERSION_MAJOR;
    uint8_t minor_version  = FW_VERSION_MINOR;
    uint8_t reserved[6]    = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
} _app_descriptor __attribute__((section(".app_descriptor")));

int main(void) {
  halInit();
  chSysInit();
  int wdt_timeout_ms = 5000;

  os::watchdog::init();
  os::watchdog::Timer wdt;
  wdt.startMSec(wdt_timeout_ms);

  chThdCreateStatic(waThread1, sizeof(waThread1), NORMALPRIO, (tfunc_t)Thread1, NULL);

  while(1) {
    chThdSleepMilliseconds(500);
    wdt.reset();
  }
}

